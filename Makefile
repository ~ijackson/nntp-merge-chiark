CFLAGS= -Wall -Wwrite-strings -Wpointer-arith -Wnested-externs -Wno-format-overflow \
	-Wmissing-declarations -Wmissing-prototypes -O3 -g
# -DDEBUG
LDFLAGS= -s
LIBS= -lident

INSTALL=install

all:		nntp-merge md5cookie1way nnrpwrap nyxpostrun

install:
		$(INSTALL) -m 755 nntp-merge nnrpwrap /usr/local/sbin/
		$(INSTALL) -m 755 md5cookie1way /usr/local/bin/

nntp-merge:	main.o post.o config.o md5.o
		$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

md5cookie1way:	sharedsecret.o md5.o
		$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

nnrpwrap:	nnrpwrap.o md5.o
		$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

nyxpostrun:	nyxpostrun.o
		$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

main.o post.o config.o:		nntp-merge.h
main.o:				md5.h
md5.o:				md5.h
