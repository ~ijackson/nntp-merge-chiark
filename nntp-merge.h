/**/

#ifndef NNTP_MERGE_H
#define NNTP_MERGE_H

#define MAX_COMMAND 2048
#define MAX_RESPONSE 2048
#define MAX_XREFLINE 5120
#define MAX_SECRET 64
#define SESAMEFILE "/etc/news/sesame"

struct permission {
  struct permission *next;
  int authd;
  char *name;
};

struct serverinfo {
  struct serverinfo *next; /* only used during setup */
  int searchthis;
  int program;
  const char *nickname;
  const char *hostname;
  unsigned short port;
  const char *send;
  FILE *rfile, *wfile, *tempfile;
};

struct groupinfo {
  struct serverinfo *readfrom;
  struct serverinfo *postto[10];
  int offset;
  struct permission *restrictto, *readonlyto;
};

struct cmdinfo {
  const char *command;
  void (*call)(char *arg, const struct cmdinfo*);
  int einfo;
};

void cmd_post(char *arg, const struct cmdinfo *cip);

void die(const char *msg);

void readconfig(void);
struct groupinfo *findgroup(const char *groupname);
struct permission *findpermit(const char *name);

void *xmalloc(size_t);
char *xstrdup(const char *);

void closeserver(struct serverinfo *server);
int stripcommand(char *buf);
int decoderesponse(char response[MAX_RESPONSE+3], unsigned long *rvp,
                   struct serverinfo *server);
int servercommand(struct serverinfo *server,
                  const char command[], char response[MAX_RESPONSE+3],
                  int flags);
void serverdataerr(struct serverinfo *si);
int copydatafile(FILE *from, FILE *to);
int stillrestricted(struct permission **pip);

extern struct permission *permissions, *restrictpost;
extern struct serverinfo *servers;
extern struct sockaddr_in peername;

extern char *myfqdn, *myxref, *lastdoneauth;
extern const char *theirfqdn;

#endif /* NNTP_MERGE_H */
