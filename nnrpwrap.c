/**/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "md5.h"

#define MAX_SECRET 64
#define NNRPDREAL "/usr/sbin/nnrpd.real"

static void chopspaces(char *buf) {
  int n;
  n= strlen(buf);
  while (n > 0 && isspace(buf[n-1])) n--;
  buf[n]= 0;
}

int main(int argc, char **argv) {
  char *p, *q, **argvs;
  unsigned char message[16+MAX_SECRET], expect[16];
  char responsebuf[300];
  char sesamebuf[MAX_SECRET*2+100];
  struct sockaddr_in peername;
  int c, n, l;
  socklen_t namesize;
  struct MD5Context md5ctx;
  FILE *file;
  unsigned long ul;
  unsigned short us;
  struct timeval timevab;

  argvs= argv;
  while ((p= *++argvs)) {
    if (*p++ != '-') break;
    c= *p++;
    if (!c) break;
    do {
      if (c == 'r') {
        if (*++argvs) {
          printf("400 %s\r\n",*argvs); exit(1);
        }
        --argvs;
        break;
      } else if (c == 's' || c == 'S') {
        if (!*++argvs) --argvs;
        break;
      }
      c= *p++;
    } while (c);
  }

  if (gettimeofday(&timevab,(void*)0)) {
    printf("400 what year is it: %s\r\n",strerror(errno));
    exit(1);
  }
  memcpy(message,&timevab.tv_sec,4);
  ul= timevab.tv_usec; memcpy(message+4,&ul,4);
  namesize= sizeof(peername);
  if (getsockname(fileno(stdin),(struct sockaddr*)&peername,&namesize) &&
      namesize == sizeof(peername) &&
      peername.sin_family == AF_INET) {
    memcpy(message+8,&peername.sin_addr,4);
    memcpy(message+12,&peername.sin_port,2);
  } else {
    memset(message+8,'x',6);
  }
  us= getpid(); memcpy(message+14,&us,2);

  printf("480 ");
  for (n=0; n<16; n++) {
    printf("%s%02x", n?":":"", message[n]);
  }
  printf("  halt !  who goes there ?\r\n");
  if (fflush(stdout) || ferror(stdout)) { perror("stdout challenge"); exit(1); }
  if (!fgets(responsebuf,sizeof(responsebuf),stdin)) {
    if (ferror(stdin)) perror("stdin response");
    else fprintf(stderr,"stdin response EOF\n");
    exit(1);
  }
  chopspaces(responsebuf);
  p= strchr(responsebuf,' ');
  if (p) *p++= 0;
  if (!strcasecmp(responsebuf,"QUIT")) {
    printf("205 please remember your papers next time.\r\n");
    exit(0);
  }
  if (strcasecmp(responsebuf,"PASS")) {
    printf("500 guards !  guards !\r\n"
           "400 this client was just leaving.\r\n");
    exit(1);
  }
  if (!p) {
    printf("501 fail.\r\n"
           "400 we don't want failures here.\r\n");
    exit(1);
  }
  file= fopen("/etc/news/sesame","r");
  if (!file) {
    printf("400 suddenly i can't see anything: %s\r\n",strerror(errno));
    exit(1);
  }
  do {
    if (!fgets(sesamebuf,sizeof(sesamebuf),file)) {
      if (ferror(file)) {
        printf("400 i'm not sure about that: %s\r\n",strerror(errno));
      } else {
        printf("400 happiness is mandatory.\r\n");
      }
      exit(1);
    }
    chopspaces(sesamebuf);
  } while (!*sesamebuf || *sesamebuf == '#' || !(q= strchr(sesamebuf,' ')));
  *q++= 0;
  l= strlen(q);
  if (l>MAX_SECRET) { printf("400 i feel all bloated.\r\n"); exit(1); }
  memcpy(message+16,q,l);

  MD5Init(&md5ctx);
  MD5Update(&md5ctx,message,16+l);
  MD5Final(expect,&md5ctx);

  for (n=0; n<16; n++) sprintf(sesamebuf+n*3,"%02x:",expect[n]);
  sesamebuf[47]= 0;
  if (!strcasecmp(p,sesamebuf)) {
    execvp(NNRPDREAL,argv);
    printf("400 o master, i have failed you: %s\r\n",strerror(errno));
    exit(1);
  }
  printf("502 impostor !\r\n"
         "400 do not darken my door again.\r\n");
  exit(1);
}
