/**/

/* Algorithm for finding a group:
 *  OUT OF DATE !
 * Start at listentry 0.
 * For each character,
 *  keep going through list until one of the following:
 *   if character in string matches in list entry,
 *    then jump to that listentry's `p' and go on to the next character
 *   if we encounter a null character (ie, default, end of this list)
 *    then we go to the listentry's `p' and use the `gi' member of
 *    the union as the group info struct
 * If we run out of characters,
 *  keep going through the list until we find a default or an if-terminate-here
 *  use the listentry's `p', and the `gi' member of that entry's union.
 *
 * The listentry pointingto's are indices into the array of listentry unions.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <ctype.h>
#include <string.h>
#include <limits.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "nntp-merge.h"

/* Conceptually we have one array with all the stuff in it.  However,
 * this is wasteful of space (we want the lookup algorithm and its
 * data to fit in 1K).  Conceptually the array is one of
 *  struct {
 *    char c;
 *    unsigned short n_to_go_to_if_char_matches_p;
 *    struct groupinfo *groupinfo_to_use_if_char_does_not_match;
 *  };
 * The groupinfo pointer is in the array element corresponding to the
 * null character at the end of the list.
 * Conceptually `end of string' is just another character, 1, except
 * that when you find it in the table you use the groupinfo member rather
 * then n_to_go member.
 */

struct tnode {
  unsigned short lip;
  struct tlistentry {
    struct tlistentry *next;
    char c;
    struct tnode *p;
  } *l;
  struct groupinfo *ifdef, *ifterm;
};

void *xmalloc(size_t s) {
  void *r;
  r= malloc(s);
  if (!r) die("malloc failed");
  return r;
}

char *xstrdup(const char *s) {
  char *r;
  r= xmalloc(strlen(s)+1);
  strcpy(r,s);
  return r;
}

struct permission *permissions;
struct permission *restrictpost;
struct serverinfo *servers;
static struct serverinfo **lastserver;

static char *lisc=0;
static unsigned short *lisp=0;
static struct groupinfo **lisg=0;

static int line;
static int nrequired;

static struct tnode *newemptytnode(void) {
  static struct tnode *r;

  r= xmalloc(sizeof(struct tnode));
  r->l= 0;
  r->ifdef= 0;
  r->ifterm= 0;
  return r;
}

static void setmissingdefaults(struct tnode *n, struct groupinfo *gi) {
  struct tlistentry *tle;
  if (n->ifdef) gi= n->ifdef; else n->ifdef= gi;
  for (tle= n->l; tle; tle= tle->next) setmissingdefaults(tle->p,gi);
}
  
static void syntax(const char *m) {
  printf("400 syntax error in config file, line %d: %s\r\n",line,m);
  exit(1);
}

static void countup(struct tnode *n) {
  struct tlistentry *tle;

  assert(n->ifdef);
  n->lip= nrequired;
  for (tle= n->l; tle; tle= tle->next) nrequired++;
  if (n->ifterm) nrequired++;
  nrequired++;

  for (tle= n->l; tle; tle= tle->next) countup(tle->p);
}

static void fillin(struct tnode *n) {
  struct tlistentry *tle;
  int i;

  i= n->lip;
  for (tle= n->l; tle; tle= tle->next) {
    lisc[i]= tle->c;
    lisp[i]= tle->p->lip;
    fillin(tle->p);
    i++;
  }
  if (n->ifterm) {
    lisc[i]= '\1';
    lisg[i]= n->ifterm;
    i++;
  }
  lisc[i]= 0;
  lisg[i]= n->ifdef;
}

static void posscopy(int *needcopyp, struct groupinfo **gip) {
  struct groupinfo *gi;
  if (!*needcopyp) return;
  gi= xmalloc(sizeof(struct groupinfo));
  *gi= **gip;
  *needcopyp= 0;
  *gip= gi;
  return;
}

struct permission *findpermit(const char *name) {
  struct permission *pi;

  for (pi= permissions; pi && strcmp(name,pi->name); pi= pi->next);
  return pi;
}

static struct permission *findmakepermit(const char *name) {
  struct permission *pi;

  if (!strcmp(name,"*")) return 0;
  pi= findpermit(name); if (pi) return pi;

  pi= xmalloc(sizeof(struct permission));
  pi->name= xstrdup(name);
  pi->authd= 0;
  pi->next= permissions;
  permissions= pi;
  return pi;
}

static struct serverinfo *findserver(const char *name) {
  struct serverinfo *search;

  for (search= servers;
       search && strcmp(search->nickname,name);
       search= search->next);

  if (!search) syntax("unknown server nickname");
  return search;
}

static struct tnode *ttree;

void readconfig(void) {
  FILE *file;
  struct groupinfo *gi;
  int needcopy= 0;
  struct tnode *tcur;
  struct tlistentry *tle;
  char buf[MAX_COMMAND], *p, *q;
  struct serverinfo *si;
  int c, i, insearchser;
  unsigned long portt;

  file= fopen("nntp-merge.conf","r");
  if (!file) die("unable to open config file");

  gi= xmalloc(sizeof(struct groupinfo));
  gi->readfrom= 0;
  gi->postto[0]= 0;
  gi->offset= 0;
  gi->restrictto= 0;
  gi->readonlyto= 0;
  needcopy= 0;

  ttree= newemptytnode();
  line= 0;
  servers= 0;
  lastserver= &servers;
  while ((c= getc(file)) != EOF) {
    line++;
    if (c == '\n') continue;
    if (c == '#') {
      while ((c= getc(file)) != EOF && c != '\n');
      continue;
    }
    if (isspace(c)) {
      if (!gi->readfrom) syntax("config file has groups before readfrom specs");
      do { c= getc(file); } while (c != EOF && c != '\n' && isspace(c));
      if (c == '\n') continue;
      ungetc(c,file);
      tcur= ttree;
      while ((c= getc(file)) != EOF && !isspace(c) && c != '*') {
        if (c == '\1') syntax("group name in config file has ^A character");
        for (tle= tcur->l; tle && tle->c != c; tle= tle->next);
        if (!tle) {
          tle= xmalloc(sizeof(struct tlistentry));
          tle->next= tcur->l;
          tle->c= c;
          tle->p= newemptytnode();
          tcur->l= tle;
        }
        tcur= tle->p;
      }
      if (c == '*') {
	tcur->ifdef= gi;
        needcopy= 1;
        c= getc(file);
      } else {
        while (c != EOF && c != '\n' && isspace(c)) c= getc(file);
        if (c != EOF && (isdigit(c) || c == '-')) {
          posscopy(&needcopy,&gi);
          ungetc(c,file);
          if (fscanf(file,"%d",&gi->offset) != 1) die("fscanf in group offset");
        } else if (gi->offset) {
          posscopy(&needcopy,&gi);
          gi->offset= 0;
        }
        tcur->ifterm= gi;
        needcopy= 1;
      }
      while (c != EOF && c != '\n') {
        c= getc(file);
        if (!isspace(c)) syntax("stuff after * or space (and offset?) in group");
      }
    } else {
      ungetc(c,file);
      if (!fgets(buf,sizeof(buf),file)) break;
      p= strchr(buf,'\n'); if (p) *p= 0;
      p= strtok(buf," \t");
      if (!strcmp(p,"read")) {
        p= strtok(0," \t"); if (!p) syntax("missing server after read");
        posscopy(&needcopy,&gi);
        gi->readfrom= findserver(p);
      } else if (!strcmp(p,"post")) {
        posscopy(&needcopy,&gi);
        for (i=0; (p= strtok(0," \t")); i++) {
          if (i >= sizeof(gi->postto)/sizeof(gi->postto[0])-1)
            syntax("too many servers after post");
          gi->postto[i]= findserver(p);
        }
        gi->postto[i]= 0;
      } else if (!strcmp(p,"server") || !strcmp(p,"server-nosearch")) {
        insearchser= !strcmp(p,"server");
        p= strtok(0," \t"); if (!p) syntax("missing server name");
        for (si= servers; si && strcmp(si->nickname,p); si= si->next);
        if (si) syntax("server nickname redefined");
        si= xmalloc(sizeof(struct serverinfo));
        si->next= 0;
        si->searchthis= insearchser;
        si->nickname= xstrdup(p);
        si->send= 0;
        p= strtok(0," \t"); if (!p) syntax("missing server host:port/command");
        if (*p == '/') {
          si->hostname= xstrdup(p);
          si->port= 0;
          si->program= 1;
        } else {
          q= strchr(p,':'); if (!q || !*q) syntax("missing :port in server");
          *q++= 0;
          si->hostname= xstrdup(p);
          portt= strtoul(q,&q,10);
          if (portt > USHRT_MAX || *q) syntax("bad :port in server");
          si->port= portt;
          si->program= 0;
        }
        while ((p= strtok(0," \t"))) {
          if (strcmp(p,"mode-reader")) syntax("bad option on server");
          si->send= "MODE READER";
        }
        si->rfile= si->wfile= 0;
        *lastserver= si;
        lastserver= &si->next;
      } else if (!strcmp(p,"permit")) {
        posscopy(&needcopy,&gi);
        p= strtok(0," \t");
        if (!p) syntax("missing read-only group after permit");
        gi->readonlyto= findmakepermit(p);
        p= strtok(0," \t");
        if (!p) syntax("missing read-write group after permit");
        gi->restrictto= findmakepermit(p);
      } else if (!strcmp(p,"myfqdn")) {
        posscopy(&needcopy,&gi);
        p= strtok(0," \t");
        if (!p) syntax("missing fqdn after myfqdn");
        free(myfqdn);
        myfqdn= xstrdup(p);
      } else if (!strcmp(p,"xref")) {
        posscopy(&needcopy,&gi);
        p= strtok(0," \t");
        if (!p) syntax("missing fqdn after myfqdn");
        free(myxref);
        myxref= xstrdup(p);
      } else if (!strcmp(p,"fetch") ||
                 !strcmp(p,"believe") ||
                 !strcmp(p,"minreaddays") ||
                 !strcmp(p,"maxperuser") ||
                 !strcmp(p,"extrarc") ||
                 !strcmp(p,"ignorerc")) {
        /* These aren't for us, we ignore them. */
      } else {
        syntax("unknown thing in file");
      }
    }
  }
  if (ferror(file)) die("failed to read config file");
  if (!ttree->ifdef) syntax("missing default newsgroup entry");
  fclose(file);

  setmissingdefaults(ttree,0);
  
  /* Now we count the total number of entries we need in our table. */
  nrequired= 0;
  countup(ttree);
  lisc= xmalloc(nrequired);
  lisp= xmalloc(nrequired*sizeof(unsigned short));
  lisg= xmalloc(nrequired*sizeof(struct groupinfo*));
  fillin(ttree);
}

struct groupinfo *findgroup(const char *name) {
  /* Null or ':' terminated */
  int c, d;
  unsigned short cnode;

  cnode= 0;
  while ((c= *name++) && c != ':' && !isspace(c)) {
    while ((d= lisc[cnode]) && d != c) cnode++;
    if (d) { cnode= lisp[cnode]; continue; }
    return lisg[cnode];
  }
  while ((d= lisc[cnode]) && d != '\1') cnode++;
  return lisg[cnode];
}
