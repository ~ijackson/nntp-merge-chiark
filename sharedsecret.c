/**/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#include "md5.h"
#define MAX_SECRET 100

static void ohshit(const char *p) {
  fprintf(stderr,"md5cookie1way: fatal error: %s\n",p); exit(1);
}

static void ohshite(const char *p) {
  fprintf(stderr,"md5cookie1way: fatal system error: %s: %s\n",p,strerror(errno));
  exit(1);
}

static void try(FILE **fp, const char *fn) {
  if (*fp) return;
  *fp= fopen(fn,"r");
}

static int parsehex(char *p, unsigned char *up, int maxlen) {
  int nused, v, n;
  
  nused= 0;
  while (nused < maxlen && (n=-1, sscanf(p,"%x%n",&v,&n) >0) && n>0) {
    p+= n;
    *up++= v;
    nused++;
    if (*p == ':') p++;
  }
  return nused;
}

int main(int argc, char **argv) {
  char *authfds, *claim, *here, *p;
  int rfd, wfd, n, nused;
  char buf[200];
  unsigned char message[16+MAX_SECRET], reply[16];
  struct MD5Context md5ctx;
  FILE *file;

  if (argc != 2) ohshit("bad args");
  claim= argv[1];

  authfds= getenv("NNTP_AUTH_FDS"); if (!authfds) ohshit("no NNTP_AUTH_FDS");
  p= strchr(authfds,'.'); if (!p) ohshit("no . in NNTP_AUTH_FDS");
  rfd= atoi(authfds);
  wfd= atoi(p+1);
  dup2(rfd,0); dup2(wfd,1);

  file= 0;
  if ((p= getenv("HOME"))) {
    sprintf(buf,"%.180s/News/md5cookies",p); try(&file,buf);
    sprintf(buf,"%.180s/.newscookies",p); try(&file,buf);
  }
  try(&file,"/etc/news/md5cookies.read");
  try(&file,"/etc/news/md5cookies");
  if (!file) ohshite("no cookies file");

  for (;;) {
    errno= 0; if (!(fgets(buf,sizeof(buf),file))) ohshite("no line for claim");
    if (!*buf || *buf == '\n' || *buf == '#' || *buf == '@') continue;
    here= strtok(buf," \t\n");
    if (!here) continue;
    if (!strcmp(here,claim)) break;
  }

  p= strtok(0," \t\n");
  if (!p) ohshit("no cookie on line");

  nused= parsehex(p,message+16,MAX_SECRET);

  errno= 0; if (!fgets(buf,sizeof(buf),stdin)) ohshite("no comm with server");
  if (strncmp(buf,"100 ",4)) {
    fprintf(stderr,"\nunable to authenticate - server sent:\n %s",buf);
    exit(1);
  }

  if (parsehex(buf+4,message,17) != 16) ohshit("server sent wrong amount of hex");

  MD5Init(&md5ctx);
  MD5Update(&md5ctx,message,16+nused);
  MD5Final(reply,&md5ctx);

  printf("MD5 ");
  for (n=0; n<16; n++) {
    printf("%s%02x", n?":":"", reply[n]);
  }
  printf("\r\n");
  fflush(stdout);

  errno= 0; if (!fgets(buf,sizeof(buf),stdin)) ohshite("no reply from server");
  if (strncmp(buf,"281",3)) ohshit("server didn't send 281");

  exit(0);
}
